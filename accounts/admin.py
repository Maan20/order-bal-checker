from django.contrib import admin
from accounts.models import *
# Register your models here.
admin.site.register(User)

@admin.register(CheckOrder)
class CheckOrderAdmin(admin.ModelAdmin):
    list_display = ("id", "remark","user_remark","total_vouchers","check_state","responsed_on","is_started","created_on")

@admin.register(CheckVoucher)
class CheckVoucherAdmin(admin.ModelAdmin):
    list_display = ("id", "code","pin","balance","expiry","order","is_checked","message","status","created_on")
