import json
from django.db.models.functions import ExtractMonth
from django.contrib.auth import get_user_model
from datetime import datetime, timedelta
from django.db.models import Sum, Count
from django.utils import timezone
from datetime import datetime
from accounts.models import *
from django import template
from django.db.models import Q



register = template.Library()

last_month = datetime.today() - timedelta(days=1)

@register.filter(name='order_status')
def order_status(key):
    try:
        order = CheckOrder.objects.get(id=key)
        vouchers = CheckVoucher.objects.filter(order_id=key)
        total_vouchers = vouchers.count()
        checked_vouchers = vouchers.filter(is_checked=True).count()
        percent = (checked_vouchers / total_vouchers) * 100
        total_percent = round(percent, 2)
        if total_percent == 100:
            color = "success"
        elif total_percent > 1:
            color = "primary"
        else:
            color = "danger"
        if total_percent == 100:
            styels= 100
            if order.is_started:
                st = "Completed"
                order.is_started=False
        elif total_percent > 1:
            styels = total_percent
            if order.is_started:
                st = "In-Progress"
            else:
                st = "Stopped"
        else:
            styels = 10
            if order.is_started:
                st = "Started"
            else:
                st = "Stopped"
            
        htm = f"""<button type="button" class="btn btn-primary py-0">
        {st} <span class="badge badge-light p-0 px-2">{total_percent}%</span>
        <span class="sr-only">unread messages</span>
        </button>
                <div class="progress mt-1">
                    <div class="progress-bar bg-{color} text-dark" role="progressbar" style="width: {styels}%"
                        aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                </div>"""
        return htm
    except:
        htm = """<div class="small">0%</div>
        <div class="progress">
            <div class="progress-bar bg-danger text-dark" role="progressbar" style="width: 10%"
                aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
        </div>"""
        return htm

from market.models import Source, Notification

@register.filter(name='report_panels')
def report_panels(key):
    panels = Source.objects.all().only("id","title")[0:4]
    return panels


@register.filter(name='clear_notifications')
def clear_notifications(key):
    Notification.objects.all().update(is_read=True)
    print("cleared")
    return "true"