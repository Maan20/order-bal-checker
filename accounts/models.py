from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from .constants import *

number = RegexValidator(
    r'^^(\+\d{1,3})?,?\s?\d{8,13}', 'Only numbers are allowed.')

"""
user model
"""


class User(AbstractUser):
    full_name       = models.CharField(_("Full Name"), max_length=150, null=True, blank=True)
    email           = models.EmailField("email address", unique=True, blank=True, null=True)
    mobile_no       = models.CharField(max_length=16, validators=[number], null=True, blank=True)
    gender          = models.IntegerField(choices=GENDER, default=MALE, null=True, blank=True)
    profile_pic     = models.FileField(upload_to="profile_pictures/%Y/%m/%d/", blank=True, null=True)
    user_type       = models.IntegerField(choices=USER_ROLE, null=True, blank=False)
    created_on      = models.DateTimeField(auto_now_add=True)
    updated_on      = models.DateTimeField(auto_now=True)

    USERNAME_FIELD  = 'email'
    REQUIRED_FIELDS = ['username']

    class Meta:
        managed = True
        db_table = 'tbl_user'

    def __str__(self):
        return str(self.email)



class CheckOrder(models.Model):
    remark          = models.CharField(max_length=100, blank=True, null=True)
    user_remark     = models.CharField(max_length=100, blank=True, null=True)
    total_vouchers  = models.PositiveIntegerField(null=False, blank=False, default=0)
    check_state     = models.IntegerField(choices=CHECKER_STATUS, default=PENDING, null=True, blank=True)
    responsed_on    = models.DateTimeField(auto_now=False,auto_now_add=False,null=True,blank=True)
    created_on      = models.DateTimeField(auto_now_add=True)
    updated_on      = models.DateTimeField(auto_now=True)
    is_started      = models.BooleanField(default=False)
    
    class Meta:
        managed = True
        ordering = ['-id']
        db_table = 'tbl_orders'
    
    

class CheckVoucher(models.Model):
    code            = models.CharField(max_length=100, blank=True, null=True)
    pin             = models.CharField(max_length=100, blank=True, null=True)
    balance         = models.FloatField(default=0, blank=True, null=True)
    expiry          = models.DateField(blank=True, null=True)
    responsed_on    = models.DateTimeField(auto_now=False,auto_now_add=False,null=True,blank=True)
    order           = models.ForeignKey(CheckOrder,blank=True,null=True,on_delete=models.DO_NOTHING,related_name="order_voucher")
    is_checked      = models.BooleanField(default=False)
    message         = models.TextField(null=True,blank=True)
    status          = models.IntegerField(choices=VOUCHER_STATUS, default=3)
    created_on      = models.DateTimeField(auto_now_add=True)
    updated_on      = models.DateTimeField(auto_now=True)
    class Meta:
        managed = True
        ordering = ['-id']
        db_table = 'tbl_voucher'
    