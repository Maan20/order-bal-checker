import os
import time
from .models import *
import datetime as dt
from datetime import datetime
from django.contrib import messages
from django.db.models import Q, Sum
from django.shortcuts import redirect, render
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from project.super_user import superuser_required, staff_required
"""
Admin login
"""
class AdminLoginView(TemplateView):
    def get(self, request, *args, **kwargs):
        return redirect('accounts:login')


"""
logout view
"""
class LogOutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('accounts:login')


"""
Login view
"""
class LoginView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'registration/login.html', {'change': "login"})

    def post(self, request, *args, **kwargs):
        agent = request.META['HTTP_USER_AGENT']
        IP = request.META.get("REMOTE_ADDR")
        des = request.path
        urls = "http://"+IP+des
        email = request.POST.get("email")
        password = request.POST.get("password")

        if not email:
            messages.error(request, 'Please enter email')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

        if not password:
            messages.error(request, 'Please enter password')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

        if request.POST.get('remember_me') == 'on':
            request.session.set_expiry(7600)

        user = authenticate(username=email, password=password)
        if not user:
            messages.error(request, 'Invalid login details.')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

        if user.is_superuser:
            login(request, user)
            return redirect('admin:index')

        login(request, user)
        return redirect('frontend:index')


"""
change password
"""
@superuser_required()
class PasswordChange(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        return render(request, 'admin/change-password.html')

    def post(self, request, *args, **kwargs):
        id = request.user.id
        user = User.objects.get(id=id)
        pass1 = request.POST.get("password")
        match = check_password(pass1, user.password)
        if match:
            messages.error(request, 'Choose A different Passsword ')
            return render(request, 'admin/change-password.html')
        else:
            user.set_password(pass1)
            user.save()
            messages.add_message(request, messages.INFO,
                                 'Password changed successfully')
            return redirect('accounts:login')


'''
Profile View
'''
@login_required
def Profile_view(request):
    return render(request, 'admin/profile.html')


'''
Edit User
'''
@superuser_required()
class EditUser(View):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = User.objects.get(id=request.user.id)
        return render(request, 'admin/edit-profile.html', {"user": user})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        user = User.objects.get(id=request.GET.get("id"))

        if request.POST.get("username"):
            username = request.POST.get("username")
            if User.objects.filter(username=username).exclude(id=user.id):
                messages.error(
                    request, 'Other User already exist with same username.')
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
            user.username = username

        if User.objects.filter(email=request.POST.get('username')):
            messages.error(request, 'please enter another username.')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

        if request.POST.get("address"):
            user.address = request.POST.get("address")

        if request.FILES.get("profile_pic"):
            ext = os.path.splitext(request.FILES.get("profile_pic").name)[1]
            valid_extensions = ['.jpg', '.png', '.JPEG', '.jpeg']
            if ext.lower() not in valid_extensions:
                messages.error(request, 'Unsupported FIle Format.')
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
            user.profile_pic = request.FILES.get("profile_pic")
        user.save()
        messages.success(request, 'Profile Updated successfully')
        return redirect('accounts:view_profile')



