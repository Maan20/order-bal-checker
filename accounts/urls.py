from django.contrib import admin
from django.urls import reverse_lazy, path
from .views import *
admin.autodiscover()

app_name = 'accounts'

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout', LogOutView.as_view(), name='logout'),
    path('profile',Profile_view, name='view_profile'),
    path('edit-profile', EditUser.as_view(), name='edit_profile'),
    path('change-password', PasswordChange.as_view(), name='Password_Change'),

    
]