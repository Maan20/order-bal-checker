
MALE = 1
FEMALE = 2
OTHER = 3
GENDER = [(MALE, "male"), (FEMALE, "female"),(OTHER, "other")]

ADMIN = 1
SUBADMIN = 2
USERS = 3
USER_ROLE = [(ADMIN, "admin"),(SUBADMIN, "sub-admin"),(USERS, "users"), ]


VOUCHER_STATUS = [(1, "Purchased"),(2, "Activated"),(3, "Not Known"), ]

COMPLETE = 1
INPROGRESS = 2
PENDING = 3
INCOMPLETED = 4
CHECKER_STATUS = [(COMPLETE, "Complete"), (INPROGRESS, "In-Progress"),(PENDING, "Pending"), (INCOMPLETED, "In-Complete")]
