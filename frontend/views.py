
import os
from django.http import HttpResponse
from django.shortcuts import render,redirect
import logging
db_logger = logging.getLogger('db') 



"""
Index Page
"""
def index(request):
    
        if request.user.is_authenticated == True and request.user.is_superuser:
            return redirect('admin:index')
        elif request.user.is_authenticated == True and request.user.is_staff:
            return render(request, "market/index.html", {'status' : 'home'})
        else:
            return render(request, "frontend/index.html", {'status' : 'home'})


def robot(request):
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	path= os.path.join(BASE_DIR)
	fin = open(f'{path}/project/robots.txt', 'rb')
	file_content = fin.read()
	fin.close()
	return HttpResponse(file_content, content_type="text/plain")


def about_us(request):
	return render(request, 'frontend/about.html', {'status' : 'about'})
def services(request):
	return render(request, 'frontend/services.html', {'status' : 'services'})

def contact_us(request):
	return render(request, 'frontend/contact.html', {'status' : 'contact'})

def gift_cards(request):
	return render(request, 'frontend/gift-cards.html', {'status' : 'cards'})



"""
error pages
"""

def error_404(request, exception):
    return render(request,'frontend/404.html')
    
def error_500(request):
    db_logger.exception("internal server error")
    return render(request,'frontend/404.html')
    
def error_400(request, exception):
    # db_logger.exception(exception)
    return render(request, 'frontend/404.html', status=404)
