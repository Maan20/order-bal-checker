from django.contrib import admin
from django.urls import reverse_lazy, path
from .views import *
from .sitemaps import *
# from django.contrib.sitemaps.views import sitemap
from django.conf import settings
from django.conf.urls.static import static


app_name = 'frontend'

# sitemaps = {
#  'pages': StaticSitemap,
# }


urlpatterns = [
    path('', index, name='index'),
    # path('sitemap.xml', sitemap, {'sitemaps': sitemaps}),
    # path('robots.txt', robot, name='robots'),
    # path('robots/',robot, name='robots'),
    path('about',about_us, name='about_us_page'),
    path('services',services, name='services_page'),
    path('gift-cards',gift_cards, name='gift_cards'),
    path('contact',contact_us, name='contact_us_page'),
    
    
    ]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
