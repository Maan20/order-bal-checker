from django.contrib import admin
from django_db_logger.models import StatusLog

admin.site.unregister(StatusLog)

class LogAdmin(admin.ModelAdmin):
    list_display =["id", "get_trace","create_datetime"]
    def get_trace(self, obj):
        if obj.trace:
            return obj.trace[-160:]
        else:
            return None
    get_trace.trace = "trace"

admin.site.register(StatusLog, LogAdmin)

