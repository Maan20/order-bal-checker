from django.contrib import admin
from .models import Source,Record, Notification


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    list_display = ("id", "title","remark","created_by","created_on")

@admin.register(Record)
class RecordAdmin(admin.ModelAdmin):
    list_display = ("id", "name","email","mobile_no","source","created_on")

@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ("id", "title","message","is_read","is_critical","created_on")

