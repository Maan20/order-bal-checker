from .views import *
from django.urls import path

app_name = 'market'

urlpatterns = [
    path('category', PanelView.as_view(), name='panels_view'),
    path('category-records/<int:pk>', CategoryRecordsView.as_view(), name='category_records'),
    path('download-category-records/<int:pk>', DownloadCategoryRecordsView.as_view(), name='download_category_records'),
    path('delete-category/<int:pk>', DeletePanelView.as_view(), name='delete_category'),
    path('delete-record/<int:pk>', DeleteRecordView.as_view(), name='delete_record'),
    path('edit-record/<int:pk>', EditRecordView.as_view(), name='edit_record'),
    
    path('records', RecordView.as_view(), name='record_page'),
    path('add-new-entry', AddRecordView.as_view(), name='add_user_record'),
    path('upload-records', RecordUploadView.as_view(), name='upload_records'),
    path('notifications', NotificationsView.as_view(), name='notifications'),
    
    
]
