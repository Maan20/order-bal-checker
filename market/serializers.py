from rest_framework import serializers
from accounts.models import *
import string
import random
from rest_framework import exceptions
from .models import Source

class SourceSerializer(serializers.ModelSerializer):
    color = serializers.SerializerMethodField(read_only=True)
    created_by = serializers.CharField(read_only=True, source="created_by.full_name")
    records = serializers.CharField(read_only=True, source="records.all.count")
    created_on = serializers.DateTimeField(read_only=True,format="%Y-%m-%d")

    def get_color(self, cat):
        try:
            red = random.randint(0, 63)
            green = random.randint(0, 63)
            blue = random.randint(0, 63)
            color = "#{:02x}{:02x}{:02x}".format(red, green, blue)
            return color
        except:
            return "#7DA0FA"
    class Meta:
        model = Source
        fields = ("__all__")        