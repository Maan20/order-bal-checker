import os
import csv
import logging
import requests
import pandas as pd
from .models import *
from tablib import Dataset
from accounts.models import *
from django.db.models import Q
from django.shortcuts import render
from django.contrib import messages
from django.views.generic import View
from datetime import datetime, timedelta
from .serializers import SourceSerializer
from project.super_user import staff_required
from .tasks import create_records_from_dataframe
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from .scheduler import scheduler

db_logger = logging.getLogger('db')

@staff_required()
class PanelView(View):
    def get(self, request, *args, **kwargs):
        panels=Source.objects.all().order_by("-id")
        data = SourceSerializer(panels,many=True).data
        return render(request, 'market/panels.html',{"panels":data,"status":"panels"})

    def post(self, request, *args, **kwargs):
        title = request.POST.get("title")
        remark = request.POST.get("remark")
        if not title or not remark:
            messages.error(request, 'Enter title and remark')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        if Source.objects.filter(title__icontains = title):
            messages.error(request, 'Category with same title already exist')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        panel=Source.objects.create(title = title,remark = remark,created_by = request.user)
        Notification.objects.create(title="One new panel added.",
                    message = f"One new panel/category added with name <i>{title}<i>")
        messages.success(request, 'Created Successfully')
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

@staff_required()
class CategoryRecordsView(View): 
    def get(self, request,pk):
        try:
            panel=Source.objects.get(id=pk)
            records = Record.objects.filter(source_id = pk).order_by("-created_on")
            query = ""
            if request.GET.get("q"):
                query = request.GET.get("q")
                try:
                    records=records.filter(Q(id=query)|Q(name__icontains=query)|Q(email__icontains=query)|Q(mobile_no=query),source_id = pk).order_by("-created_on").distinct()
                    if not records:
                        raise
                except:
                    records=records.filter(Q(name__icontains=query)|Q(email__icontains=query)|Q(mobile_no=query),source_id = pk).order_by("-created_on").distinct()
            else:
                records = Record.objects.filter(source_id = pk).order_by("-created_on")

            page = request.GET.get('page', 1)
            paginator = Paginator(records,8)

            try:
                records = paginator.page(page)
            except PageNotAnInteger:
                records = paginator.page(1)
            except EmptyPage:
                records = paginator.page(paginator.num_pages)
            return render(request, 'market/cat-records.html',{"records":records,"query":query,"panel":panel,"status":"all_records"})

        except Exception as e:
            print(e)
            messages.error(request, 'Category not found')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


@staff_required()
class RecordView(View):
    def get(self, request, *args, **kwargs):
        query = ""
        if request.GET.get("q"):
            query = request.GET.get("q")
            try:
                records=Record.objects.filter(Q(id=query)|Q(name__icontains=query)|Q(email__icontains=query)|Q(mobile_no=query)).order_by("email","name").distinct()
                if not records:
                    raise
            except:
                records=Record.objects.filter(Q(name__icontains=query)|Q(email__icontains=query)|Q(mobile_no=query)).order_by("email","name").distinct()
        else:
            records=Record.objects.all().order_by("email","name")

        page = request.GET.get('page', 1)
        paginator = Paginator(records,20)

        try:
            records = paginator.page(page)
        except PageNotAnInteger:
            records = paginator.page(1)
        except EmptyPage:
            records = paginator.page(paginator.num_pages)
        return render(request, 'market/records.html',{"records":records,"query":query,"last_page":paginator.num_pages,"status":"all_records"})



@staff_required()
class NotificationsView(View):
    def get(self, request, *args, **kwargs):
       
        notifications=Notification.objects.all().order_by("-created_on")
        page = request.GET.get('page', 1)
        paginator = Paginator(notifications,100)
        try:
            notifications = paginator.page(page)
        except PageNotAnInteger:
            notifications = paginator.page(1)
        except EmptyPage:
            notifications = paginator.page(paginator.num_pages)

        return render(request, 'market/notifications.html',{"notifications":notifications,"status":"notifications"})


@staff_required()
class AddRecordView(View):
    def get(self, request, *args, **kwargs):
        panels=Source.objects.all().order_by("-id")
        return render(request, 'market/add-records.html',{"panels":panels,"status":"add_record"})
    def post(self, request, *args, **kwargs):
        name = request.POST.get("name")
        email = request.POST.get("email")
        mobile_no = request.POST.get("mobile_no")
        remark = request.POST.get("remark")
        category_id = request.POST.get("category_id")
        print(name,email,category_id)
        if not name or not email or not category_id:
            messages.error(request, 'Enter name and email and category')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        try:
            category = Source.objects.get(id=category_id)
        except:
            messages.error(request, 'User Record with same email already exist')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        
        if Record.objects.filter(mobile_no = mobile_no):
            messages.error(request, 'User Record with same mobile no already exist')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

        if Record.objects.filter(email__iexact = email):
            messages.error(request, 'User Record with same email already exist')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
         
        user=Record.objects.create(
            name = name,
            email = email,
            mobile_no = mobile_no,
            remark = remark,
            source = category,
            created_by=request.user
        )
        Notification.objects.create(title="One recorded added manually",
                            message = f"Record created manually in {category.title} panel")
        messages.success(request, 'Created Successfully')
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))



'''
Upload Excel
'''    
# @csrf_exempt
class RecordUploadView(View):
    def post(self, request):    
        filename = request.FILES.get('expfile')
        category_id = request.POST.get("category_id")
        remark = request.POST.get("remark")
        if not filename:
            messages.success(request, 'File not found')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        try:
            category = Source.objects.get(id=category_id)
        except:
            messages.error(request, 'Category not found')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        excel_data_df = pd.read_excel(filename)
        scheduler.add_job(create_records_from_dataframe, trigger="date", args=[excel_data_df,remark,category,request.user], run_date=datetime.now() + timedelta(seconds=1))
        Notification.objects.create(title="Excel file recieved",
        message = "Records will create in few minutes.")

        messages.success(request, 'Will be uploaded in few minutes, Please check notification tab for updates.')
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


@staff_required()
class DownloadCategoryRecordsView(View):
    def get(self, request,pk):
        try:
            panel=Source.objects.get(id=pk)
            data = panel.records.values_list("id","name","email","mobile_no","remark","source__title","source_id","created_by__full_name","created_on__date")
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = f'attachment; filename=Credencerewards-{panel.title}-user-records.csv'
            csv_writer = csv.writer(response)
            csv_writer.writerow(["RecordId","Name","Email","Mobile_No","Remark","CategoryTitle","categoryId","CreatorName","Dated"])
            csv_writer.writerows(data)
            return response
        except:
            records = Record.objects.all()
            data = records.values_list("id","name","email","mobile_no","remark","source__title","source_id","created_by__full_name","created_on__date")
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename=Credencerewards-all-user-records.csv'
            csv_writer = csv.writer(response)
            csv_writer.writerow(["RecordId","Name","Email","Mobile_No","Remark","CategoryTitle","categoryId","CreatorName","Dated"])
            csv_writer.writerows(data)
            return response


@staff_required()
class DeletePanelView(View):
    def get(self, request,pk):
        try:
            panel=Source.objects.get(id=pk)
            if len(panel.records.all()) == 0:
                panel.delete()
                messages.success(request, "Deleted suucessfully")
            else:
                messages.error(request, "can't delete unempty category")
        except:
            pass
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))


@staff_required()
class DeleteRecordView(View):
    def get(self, request,pk):
        try:
            record=Record.objects.get(id=pk)
            record.delete()
            messages.success(request, "Deleted suucessfully")
        except:
            pass
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

@staff_required()
class EditRecordView(View):
    def get(self, request,pk):
        try:
            panels=Source.objects.all().order_by("-id")
            record=Record.objects.get(id=pk)
            return render(request, 'market/edit-record.html',{"record":record,"panels":panels,"status":"all_records"})
        except:
            pass
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
    def post(self, request, pk):
        try:
            record=Record.objects.get(id=pk)
            name = request.POST.get("name")
            email = request.POST.get("email")
            mobile_no = request.POST.get("mobile_no")
            remark = request.POST.get("remark")
            category_id = request.POST.get("category_id")
            print(name,email,category_id)
            if not name or not email or not category_id:
                messages.error(request, 'Enter name and email and category')
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
            try:
                category = Source.objects.get(id=category_id)
            except:
                messages.error(request, 'User Record with same email already exist')
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
            
            if Record.objects.filter(mobile_no = mobile_no).exclude(id=record.id):
                messages.error(request, 'User Record with same mobile no already exist')
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

            if Record.objects.filter(email__iexact = email).exclude(id=record.id):
                messages.error(request, 'User Record with same email already exist')
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
            
            
            record.name = name
            record.email = email
            record.mobile_no = mobile_no
            record.remark = remark
            record.source = category
            record.created_by=request.user
            record.save()
            messages.success(request, 'Updated Successfully')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        except:
            messages.success(request, 'Something went wrong')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))

def update_result_to_db(result,panel):
    if result:
        user= User.objects.filter(id=4)
        user=user.first()
        for data in result:
            name = data.get("name")
            email = data.get("email")
            mobile_no = data.get("mobile_no")
            if not Record.objects.filter(email=email):
                Record.objects.create(
                name = name,
                email = email,
                mobile_no = mobile_no,
                remark = "auto-updated",
                source = panel,
                created_by=user
                )


def extract_data_from_lifestyle( lifestyle_url="https://api.credencerewards.co.in/lifestyle/market-transaction"):
    panel=Source.objects.get(id=1)
    panel.save()
    try:
        response = requests.request("GET", lifestyle_url)
    except requests.exceptions.ConnectTimeout as e:
        extract_data_from_lifestyle(lifestyle_url) 
            
    response  = response.json()
    next_page = response.get("next")
    result = response.get("results")
    update_result_to_db(result,panel)
    print(next_page)
    print(response)
    if next_page:
        extract_data_from_lifestyle(lifestyle_url=next_page)
        

def extract_data_from_axis( axis_bank_url="https://api-v2.credencerewards.co.in/api/market-transaction"):
    panel=Source.objects.get(id=2)
    panel.save()
    try:
        response = requests.request("GET", axis_bank_url)
    except requests.exceptions.ConnectTimeout as e:
        extract_data_from_axis(axis_bank_url)         
    response  = response.json()
    next_page = response.get("next")
    result = response.get("results")
    update_result_to_db(result,panel)
    print(next_page)
    print(response)
    if next_page:
        extract_data_from_axis(next_page)

def extract_data_from_shoppersstop(shoppersstop_url="https://api.credencerewards.co.in/shoppers-stop/market-transaction"):
    panel=Source.objects.get(id=3)
    panel.save()
    try:
        response = requests.request("GET", shoppersstop_url)
    except requests.exceptions.ConnectTimeout as e:
        extract_data_from_shoppersstop(shoppersstop_url)         
    response  = response.json()
    next_page = response.get("next")
    result = response.get("results")
    update_result_to_db(result,panel)
    print(next_page)
    print(response)
    if next_page:
        extract_data_from_shoppersstop(next_page)

def extract_data_from_website(website_url="https://panel.credencerewards.com/api/brands/market-transaction"):
    panel=Source.objects.get(id=4)
    panel.save()
    try:
        response = requests.request("GET", website_url)
    except requests.exceptions.ConnectTimeout as e:
        extract_data_from_website(website_url)         
    response  = response.json()
    next_page = response.get("next")
    result = response.get("results")
    update_result_to_db(result,panel)
    print(next_page)
    print(response)
    if next_page:
        extract_data_from_website(next_page)


def extract_data_from_cr_code(cr_code_url="https://cr-code.credencerewards.com/api/market-transaction"):
    panel=Source.objects.get(id=5)
    panel.save()
    try:
        response = requests.request("GET", cr_code_url)
    except requests.exceptions.ConnectTimeout as e:
        extract_data_from_cr_code(cr_code_url)         
    response  = response.json()
    next_page = response.get("next")
    result = response.get("results")
    update_result_to_db(result,panel)
    print(next_page)
    print(response)
    if next_page:
        extract_data_from_cr_code(next_page)


def extract_data_from_axis_1(url="https://api-v2.credencerewards.co.in/axis1/market-transaction"):
    panel=Source.objects.get(id=6)
    panel.save()
    try:
        response = requests.request("GET", url)
    except requests.exceptions.ConnectTimeout as e:
        print(e)
        extract_data_from_axis_1(url)         
    response  = response.json()
    next_page = response.get("next")
    result = response.get("results")
    update_result_to_db(result,panel)
    print(next_page)
    print(response)
    if next_page:
        extract_data_from_axis_1(next_page)

