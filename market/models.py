from django.db import models
from django.core.validators import RegexValidator

number = RegexValidator(
    r'^^(\+\d{1,3})?,?\s?\d{8,13}', 'Only numbers are allowed.')
class Source(models.Model):
    title       = models.CharField(max_length=100, null=True, blank=False)
    remark      = models.CharField(max_length=200, null=True, blank=False)
    is_active   = models.BooleanField(default=True)
    created_by  = models.ForeignKey("accounts.User",related_name="category",on_delete=models.DO_NOTHING)
    created_on  = models.DateTimeField(auto_now_add=True, null=True)
    updated_on  = models.DateTimeField(auto_now=True)

    def _str_(self):
        return self.title

class Record(models.Model):
    name            = models.CharField(max_length=300, blank=True, null=True)
    email           = models.CharField(max_length=500, blank=True, null=True)
    mobile_no       = models.CharField(max_length=30, null=True, blank=True)

    remark          = models.CharField(max_length=50, blank=True, null=True)
    is_active       = models.BooleanField(default=True)
    source          = models.ForeignKey(Source,related_name="records",on_delete=models.DO_NOTHING)
    created_by      = models.ForeignKey("accounts.User",null=True,blank=True,related_name="user_record",on_delete=models.DO_NOTHING)

    created_on      = models.DateTimeField(auto_now_add=True)
    updated_on      = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['id']


class Notification(models.Model):
    title           = models.CharField(max_length=100, blank=True, null=True)
    message         = models.TextField(null=True,blank=True)
    is_critical     = models.BooleanField(default=False)
    is_read         = models.BooleanField(default=False)
    created_on      = models.DateTimeField(auto_now_add=True)
    class Meta:
        ordering = ['-id']