# __init__.py
from .scheduler import scheduler

# Start the scheduler when the app is loaded
scheduler.start()
