from .models import Record, Notification
from datetime import datetime
import logging
db_logger = logging.getLogger('db')
def create_records_from_dataframe(dataframe,remark,category,user):
    Notification.objects.create(title="Excel file start Uploading",
                                message = "File uploading and started record in Database")
    skipped_values = []
    i = 0
    for index, row in dataframe.iterrows():
        try:
            if not Record.objects.filter(email=str(row['Email'])[0:256]).exists():
                try:
                    mobile_no=str(row['Mobile_No'])[0:20]
                except:
                    mobile_no = "--"
                Record.objects.create(
                        name=str(row['Name'])[0:200],
                        email=str(row['Email'])[0:256],
                        mobile_no=mobile_no,
                        remark = remark,
                        source = category,
                        created_by=user
                    )
                i+=1
            else:
                skipped_values.append(str(row['Email'])[0:256])
        except Exception as e:
            Notification.objects.create(title="Some error occured",
                                        message = str(e),is_critical=True)
    
    Notification.objects.create(title=f"{i} records created",
                            message = f" File uploaded and {i} records created in Database in {category.title} Panel")

    if len(skipped_values):
        Notification.objects.create(title=f"{len(skipped_values)} Duplicate record found",
                                    message = f"Duplicate Records : {tuple(skipped_values)}")
