# CredenceRewards AdminPanel for Amazon
## Guidlines from CredenceRewards Backend Development Team
----------------------------------------------------------------------------------------
## Setup

The first thing to do is to clone the repository:

```sh
$ git clone git-repo-url.git
$ cd credence-admin-project
```

Create a virtual environment to install dependencies in and activate it:

```sh
$ virtualenv2 --no-site-packages env
$ source env/bin/activate
```

Then install the dependencies:

```sh
(env)$ pip install -r requirements.txt
```
Note the `(env)` in front of the prompt. This indicates that this terminal
session operates in a virtual environment set up by `virtualenv2`.

Once `pip` has finished downloading the dependencies:
```sh
(env)$ cd project
(env)$ python manage.py runserver
```
And navigate to `http://127.0.0.1:8000/admin/`.

In order to create admin go to terminal and create superuser in

```sh
(env)$ python3 manage.py createsuperuser
(env)$ python manage.py runserver
```

## Walkthrough

Before you interact with the application, go login page and set up
the Redirect URI in the Developer settings. To make it work with this
application, use the value `http://127.0.0.1:8000/accounts/login/`. This is to
make sure you are redirected back to your site where the your account is verified.

## Webhooks

Set up `localtunnel` to test out Webhooks. The `localtunnel` package should be
installed as a dependency to the project.
Note, however, that the port number is the same as the port that `python manage.py runserver` is
running on, which is 8000.
```sh
(env)$ localtunnel-beta 8000
=> Port 8000 is now publicly accessible from http://5bebd69e5222.v2.localtunnel.com ...
```
Please refer to the [the Webhooks manual](https://sandbox.gocardless.com/docs/python/merchant_tutorial_webhook#receiving-webhooks) for more details.


# Some importtant commands
# credence

# Prerequisites
 To run this project you must have installed these Packages and dependencies.
	
 1.Python3.
		
	
	sudo apt-get install python3
	
 2.pip3.
	
	
	sudo apt-get install python3-pip
	
	
## Installation

Use the package manager pip to install requirement.txt.


	pip3 install -r requirement.txt --user

# Getting Started
	
1. Remove migrations from all apps.
	
2. Run this command for makemigrations.
		
	python3 manage.py makemigrations
			
3. Run this command for migrate 
		
	python3 manage.py migrate
			
4. Run this command to create superuser.
		
	python3 manage.py createsuperuser
			

			

# Running the project:

To run this project run this command on your terminal:

   python3 manage.py runserver