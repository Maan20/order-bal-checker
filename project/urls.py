from django.contrib import  admin
from django.views.static import serve
from django.urls import include, path , re_path
from accounts.views import  AdminLoginView
from django.conf.urls.static import static
from frontend import views as frontend_views
from django.conf import settings

urlpatterns = [
    path('admin/login/', AdminLoginView.as_view()),
    path('admin/', admin.site.urls),
    path('', include('frontend.urls')),

    path('', include('accounts.urls', 'accounts')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('dblogger/', include('dblogger.urls') , name="dblogger"),
    path('check/', include('balancechecker.urls') , name="balancechecker"),
    path('marketting/', include('market.urls') , name="market"),
    
    re_path(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}), 
    re_path(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}), 
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

handler404 = frontend_views.error_400
handler500 = frontend_views.error_500
handler400 = frontend_views.error_404
handler403 = frontend_views.error_404