from django.db import models

# Create your models here.

class ExtraVoucher(models.Model):
    code            = models.CharField(max_length=100, blank=True, null=True)
    pin             = models.CharField(max_length=100, blank=True, null=True)
    balance         = models.FloatField(default=0, blank=True, null=True)
    expiry          = models.DateField(blank=True, null=True)
    created_on      = models.DateTimeField(auto_now_add=True)
    updated_on      = models.DateTimeField(auto_now=True)
    class Meta:
        managed = True
        ordering = ['-id']