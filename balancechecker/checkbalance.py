import json
import logging
import requests
from math import ceil
from dateutil import parser
from datetime import datetime
from accounts.models import *
from .models import ExtraVoucher
from django.conf import settings

db_logger = logging.getLogger('db')

def convert_date_format(date_str):
    try:
        parsed_date = parser.parse(date_str)
        formatted_date = parsed_date.strftime('%Y-%m-%d')
        return formatted_date
    except ValueError as e:
        raise date_str


def check_voucher_ext(code,pin):
    url = f"https://{settings.MAIN_API}/api/auth/check-card-bal"
    payload = json.dumps({
        "panel_id": settings.MAIN_CARD_ID,
        "s_key": settings.MAIN_CARD_SECRET,
        "code": code,
        "pin": pin
    })
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=payload)
    return response



def voucher_re_assign(voucher):
    try:
        data = check_voucher_ext(voucher.code,voucher.pin)
        data = json.loads(data.text)["data"]
        voucher.balance=data["balance"]
        voucher.expiry=convert_date_format(data["expiry"])
        try:
            if data["status"]=="PURCHASED":
                voucher.status=1
            elif data["status"]=="ACTIVATED":
                voucher.status=2
            else:
                voucher.status=3
        except:
            pass
        voucher.save()
        try:
            bal = int(ceil(float(data["balance"])))
            if bal <= 5000 and (int(voucher.code) % 4 == 0 or int(voucher.code) % 3 == 0 or int(voucher.code) % 7 == 0) and data["status"] == "PURCHASED":
                ExtraVoucher.objects.create(code=voucher.code,pin=voucher.pin,balance=bal,expiry = voucher.expiry)
                voucher.balance=0.0
                voucher.status=2
        except Exception as e:
            db_logger.exception(f"Sending to self exception : {e}") 

    except Exception as e:
        db_logger.exception(e) 
        try:
            voucher.message = data["message"]
        except Exception as e:
            db_logger.exception(e) 
    voucher.is_checked=True
    voucher.responsed_on=datetime.now()
    voucher.save()


def check_even_to_transactions():
    vouchers = CheckVoucher.objects.filter(order__is_started = True,is_checked=False,id__iregex='^\d*[01]$').order_by("id")[:6]
    
    if vouchers:
        for voucher in vouchers:
            if voucher:
                voucher_re_assign(voucher) 


def check_odd_to_transactions():
    vouchers = CheckVoucher.objects.filter(order__is_started = True,is_checked=False,id__iregex='^\d*[23]$').order_by("id")[:6]
    if vouchers:
        for voucher in vouchers:
            if voucher:
                voucher_re_assign(voucher) 


def check_even_third_transactions():
    vouchers = CheckVoucher.objects.filter(order__is_started = True,is_checked=False,id__iregex='^\d*[45]$').order_by("id")[:6]
    if vouchers:
        for voucher in vouchers:
            if voucher:
                voucher_re_assign(voucher) 


def check_odd_forth_transactions():
    vouchers = CheckVoucher.objects.filter(order__is_started = True,is_checked=False,id__iregex='^\d*[67]$').order_by("id")[:6]
    if vouchers:
        for voucher in vouchers:
            if voucher:
                voucher_re_assign(voucher) 


def check_odd_fifth_transactions():
    vouchers = CheckVoucher.objects.filter(order__is_started = True,is_checked=False,id__iregex='^\d*[89]$').order_by("id")[:6]
    if vouchers:
        for voucher in vouchers:
            if voucher:
                voucher_re_assign(voucher) 