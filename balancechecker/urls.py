from django.urls import path
from .views import *


app_name = 'balance_checker'


urlpatterns = [
    path('', IndexView.as_view(), name="check_index"),
    path('vouchers/<int:id>', OrderVouchersView.as_view(), name="order_vouchers"),
    path('upload-vouchers', UploadCSV.as_view(), name="upload_vouchers"),
    path('download-order-voucher', DownloadOrderVoucher.as_view(), name='download_order_voucher'),
    path('start-order-process/<int:id>', StartOrderProcess.as_view(), name='start_order_process'),
    path('stop-order-process/<int:id>', StopOrderProcess.as_view(), name='stop_order_process'),

    path('api/get-data/', get_data, name='get_data'),
]

