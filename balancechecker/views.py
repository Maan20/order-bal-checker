import os
import xlwt
import logging
from tablib import Dataset
from accounts.models import *
from datetime import datetime
from django.db.models import Q
from django.shortcuts import render
from django.contrib import messages
from openpyxl import Workbook,load_workbook
from django.http import HttpResponse,JsonResponse
from django.utils.crypto import get_random_string
from project.super_user import superuser_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.generic import View
from django.utils import timezone
from .models import *

db_logger = logging.getLogger('db')

@superuser_required()
class IndexView(View):
    def get(self, request, *args, **kwargs):
        query = ""
        if request.GET.get("q"):
            query = request.GET.get("q")
            try:
                orders=CheckOrder.objects.filter(Q(id=query)|Q(user_remark__icontains=query)|Q(responsed_on__icontains=query)).order_by("-id").distinct()
                if not orders:
                    raise
            except:
                orders=CheckOrder.objects.filter(Q(user_remark__icontains=query)|Q(responsed_on__icontains=query)|Q(order_voucher__code__contains=query)|Q(order_voucher__pin__contains=query)|Q(order_voucher__expiry__contains=query)).order_by("-id").distinct()
        else:
            orders=CheckOrder.objects.all().order_by("-id")
            
        page = request.GET.get('page', 1)
        paginator = Paginator(orders,10)

        try:
            orders = paginator.page(page)
        except PageNotAnInteger:
            orders = paginator.page(1)
        except EmptyPage:
            orders = paginator.page(paginator.num_pages)
        return render(request, 'check-balance/orders.html',{"orders":orders,"query":query})


@superuser_required()
class OrderVouchersView(View):
    def get(self, request, id):
        try:
            order = CheckOrder.objects.get(id=id)
        except:
            messages.error(request, 'Order not found.')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        query = ""
        if request.GET.get("q"):
            query = request.GET.get("q")
            try:
                vouchers=CheckVoucher.objects.filter(Q(id=query)|Q(code=query)|Q(balance__icontains=query),order_id=id).order_by("-id").distinct()
                if not vouchers:
                    raise
            except:
                try:
                    vouchers=CheckVoucher.objects.filter(Q(code=query)|Q(pin__icontains=query)|Q(expiry=query)|Q(message__icontains=query)|Q(responsed_on=query),order_id=id).order_by("-id").distinct()
                except:
                    vouchers=CheckVoucher.objects.filter(Q(code=query)|Q(pin__icontains=query)|Q(message__icontains=query),order_id=id).order_by("-id").distinct()
                    
        else:
            vouchers = CheckVoucher.objects.filter(order_id=id)
        try:
            amount = vouchers.filter(status=1).values_list("balance",flat=True)
            total_amount = sum(amount)
        except:
            total_amount = 0
        vouchers=vouchers.order_by("id")
        order.total_vouchers =vouchers.count()
        order.save()
        page_count = 50 
        if vouchers.count() > 8000:
            page_count = 200
        elif vouchers.count() > 4000:
            page_count = 100
        else:
            page_count = 50
        checked_vouchers =  vouchers.filter(is_checked=True).count()
        unchecked_vouchers =  vouchers.exclude(is_checked=True).count()
        
        page = request.GET.get('page', 1)
        paginator = Paginator(vouchers, page_count)
        try:
            vouchers = paginator.page(page)
        except PageNotAnInteger:
            vouchers = paginator.page(1)
        except EmptyPage:
            vouchers = paginator.page(paginator.num_pages)

        return render(request, 'check-balance/vouchers.html',{"vouchers":vouchers,"last_page":paginator.num_pages,"order":order,
                                                    "checked_vouchers":checked_vouchers,"unchecked_vouchers":unchecked_vouchers,
                                                    "id":id,"page_count":page_count,"total_amount":total_amount})

@superuser_required()
class StartOrderProcess(View):
    def get(self, request, id):
        try:
            order = CheckOrder.objects.get(id=id)
        except:
            messages.error(request, 'Order not found.')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        order.is_started = True
        order.save()
        messages.success(request, 'Order Started Successfully.')
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
    
@superuser_required()
class StopOrderProcess(View):
    def get(self, request, id):
        try:
            order = CheckOrder.objects.get(id=id)
        except:
            messages.error(request, 'Order not found.')
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        order.is_started = False
        order.save()
        messages.success(request, 'Order Stopped Successfully.')
        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))    

@superuser_required()
class UploadCSV(View):
    def post(self, request):    
        try:
            filename = request.FILES.get('expfile')
            ext = os.path.splitext(filename.name)[1]
            valid_extensions = ['.xls', '.xlsx']
            validformat=['code', 'pin']
            validformatcp=['Code', 'Pin']
            if not ext.lower() in valid_extensions:
                messages.error(request, 'Unsupported File Format.')
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
            try:
                excel_ext=['.xls', '.xlsx']
                if ext.lower() in excel_ext:
                    dataset = Dataset()
                    try:
                        imported_data = dataset.load(filename.read(),format='xlsx')
                    except:
                        imported_data = dataset.load(filename.read(),format='xls')
                    if imported_data.headers[:2]==validformat or imported_data.headers[:2]==validformatcp:

                        order = CheckOrder.objects.create(
                            remark = get_random_string(length=10),
                            user_remark = request.POST.get("remark"),
                        )
                        i=0
                        
                        for data in imported_data:
                            i=i+1
                            if i > 10000:
                                break
                            else:
                                if len(data)>=2 and data[0] and data[1]:
                                    Fullname =str(data[0])
                                    Email = str(data[1])
                                    voucher = CheckVoucher.objects.create(
                                            code=Fullname,
                                            pin=Email, 
                                            order = order,
                                            )
                        order.total_vouchers = i
                        order.save()
                        messages.success(request,"Created Successfully!!..")
                        return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
                    
                    messages.error(request, "Format  should be: [Code, Pin]" )
                    return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
                
                messages.error(request, "Format  should be: [Code, Pin]" )
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
            except:
                messages.error(request, "Format  should be: [Code, Pin] and check file extension as .xlsx" )
                return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        except Exception as e:
            db_logger.exception(e)
            messages.error(request, "Format  should be: [Code, Pin] and check file extension as .xlsx" )
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))



from django.db.models import Case, IntegerField, When, Value, CharField

"""
Download Order Voucher
"""
@superuser_required()
class DownloadOrderVoucher(View):
    def post(self, request):   
        if not request.POST.get("order_id"):
            messages.error(request, "Order Id not found" )
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        try:
            order = CheckOrder.objects.get(id=request.POST.get("order_id"))
        except:
            messages.error(request, "Order not found" )
            return HttpResponseRedirect(request.META.get("HTTP_REFERER"))
        
        vouchers = order.order_voucher.all()
        
        expiryfrom = request.POST.get("expiryfrom")
        expiryto = request.POST.get("expiryto")
        checked = int(request.POST.get("checked"))

        if checked == 2 or  checked == "2":
            vouchers = vouchers.filter(is_checked=True)
        elif checked == 3 or  checked == "3":
            vouchers = vouchers.filter(is_checked=False)
        if checked == 2 or checked == "2" or checked == 1 or checked == "1":
            if expiryfrom and expiryto:
                vouchers = vouchers.filter( expiry__gte=expiryfrom, expiry__lte=expiryto)
            elif expiryfrom:
                vouchers = vouchers.filter( expiry__gte=expiryfrom)
            elif expiryto:
                    vouchers = vouchers.filter(expiry__lte=expiryto)

        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = f'attachment; filename="Credence-Voucher-{order.user_remark}"-"{order.id}.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet(order.remark)
        ws.col(0).width = 3000
        ws.col(1).width = 5000
        ws.col(2).width = 6000
        ws.col(5).width = 3000
        ws.col(6).width = 4000
        ws.col(7).width = 8000
        ws.col(8).width = 4000
        ws.col(9).width = 14000
        row_num = 0

        font_style = xlwt.XFStyle()
        font_style.font.bold = True
        al = xlwt.Alignment()
        al.horz = al.HORZ_CENTER
        font_style.alignment = al

        columns = ["Voucher ID", "Credence Code", "Voucher Pin", "Balance","Expiry","Checked","status",
                        "Checked On","Uploaded On","Message"]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)


        font_style = xlwt.XFStyle()
        
        al = xlwt.Alignment()
        al.horz = al.HORZ_CENTER
        font_style.alignment = al

        rows = vouchers.filter(order=order).order_by("id").annotate(
            display_status=Case(
                When(status=1, then=Value('Purchased')),
                When(status=2, then=Value('Activated')),
                default=Value('Unknown'),
                output_field=CharField()
            )
        ).values_list("id", "code","pin",
                        "balance", "expiry", "is_checked","display_status", "responsed_on", "created_on__date","message")
        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, str(row[col_num]), font_style)
        wb.save(response)
        return response
    


def get_data(request):
    # Get the 'date' parameter from the request
    date_str = request.GET.get('date', None)

    if date_str:
        try:
            date = datetime.strptime(date_str, '%Y-%m-%d').date()
        except ValueError:
            return JsonResponse({'error': 'Invalid date format. Use YYYY-MM-DD.'}, status=400)
    else:
        date = timezone.now().date()

    data = ExtraVoucher.objects.filter(expiry=date) 
    data_list = list(data.values('id','code','pin',"balance",'expiry'))  
    return JsonResponse(data_list, safe=False)


# voucher_list=
# from .checkbalance import *
# for v in voucher_list:
#     d = check_voucher_ext(v[0],v[1])    
#     data = json.loads(d.text)["data"]
#     try:
#         if data["status"] == "PURCHASED":
#             print(data)
#             print(v[1])
#             print(v[0])
#             # print(v[2])
#             print("------------------------")
#         else:
#             print("--------")
#     except:
#         pass
