from django.apps import AppConfig


class BalancecheckerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'balancechecker'
